// References:
// http://enos.itcollege.ee/~ylari/I231/Node.java
// http://enos.itcollege.ee/~jpoial/algorithms/trees.html
// https://git.wut.ee/i231/home5/src/master/src/Node.java

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class Node {
   // Node name.
   private String name;
   // First child.
   private Node firstChild;
   // Next sibling.
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   public static Node parsePostfix (String s) {
      if (s.contains("()")) {
         throw new RuntimeException("This string can't be empty.");
      } else if (s.contains("\t")) {
         throw new RuntimeException("This string can't contain tab. String value: " + s);
      } else if (s.contains(" ")) {
         throw new RuntimeException("This string can't contain whitespace. String value: " + s);
      } else if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException("This string can't start with brackets. String value: " + s);
      } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
         throw new RuntimeException("This string contains comma. String value: " + s);
      } else if (s.contains(",,")) {
         throw new RuntimeException("This string can't contains commas. String value: " + s);
      } else if (!s.contains("(") && !s.contains(")") && s.contains(",")) {
         throw new RuntimeException("This string can't contain comma if there aren't any brackets. String value: " + s);
      } else if (s.matches(".*\\)[a-zA-Z]\\(.*")) {
         throw new RuntimeException("This string can't contain comma in between of nodes. String value: " + s);
      } else if (s.matches(".*(\\( *\\,|\\, *\\)).*")) {
         throw new RuntimeException("This string can't contain comma in nodes. String value: " + s);
      }

      int openingBrackets = 0;
      int closingBrackets = 0;
      for (int i = 0; i < s.length(); i++) {
         if (s.charAt(i) == '(') {
            openingBrackets++;
         } else if (s.charAt(i) == ')') {
            closingBrackets++;
         }
         if (closingBrackets > openingBrackets) {
            throw new RuntimeException("There must be equal amount of opening and closing brackets. " +
                    "Closing bracket cannot be before opening bracket. String value: " + s);
         }
      }
      if (closingBrackets != openingBrackets) {
         throw new RuntimeException("There must be equal amount of opening and closing brackets. String value: " + s);
      }
      if (closingBrackets != openingBrackets) {
         throw new RuntimeException("There must be equal amount of opening and closing brackets. String value: " + s);
      }

      String[] t = s.split("");
      Stack<Node> stack = new Stack<Node>();
      Node node = new Node(null, null, null);
      boolean replacingRoot = false;
      for (int i = 0; i < t.length; i++) {
         String p = t[i].trim();
         if (p.equals("(")) {
            stack.push(node);
            node.firstChild = new Node(null, null, null);
            node = node.firstChild;
            if (t[i+1].trim().equals(",")) {
               throw new RuntimeException("There is a comma after node.");
            }
         } else if (p.equals(")")) {
            node = stack.pop();
            if (stack.size() == 0) {
               replacingRoot = true;
            }
         } else if (p.equals(",")) {
            if (replacingRoot) {
               throw new RuntimeException("Trying to replace root.");
            }
            node.nextSibling = new Node(null, null, null);
            node = node.nextSibling;
         } else {
            if (node.name == null) {
               node.name = p;
            } else {
               node.name += p;
            }
         }
      }
      return node;

   }

   public String leftParentheticRepresentation() {

      StringBuffer b = new StringBuffer();
      b.append(this.name);
      if (firstChild != null) {
         b.append("(");
         b.append(firstChild.leftParentheticRepresentation());
         b.append(")");
      }

      if (nextSibling != null) {
         b.append(",");
         b.append(nextSibling.leftParentheticRepresentation());
      }
      return b.toString();

   }

   public static void main (String[] param) {
      String s = "((r)t,(C,)u)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}